import * as Joi from '@hapi/joi';
import * as dotenv from 'dotenv';

dotenv.config({ path: '.env.default' });

const config = {
    environment: process.env.NODE_ENV as any,
    api: {
        host: process.env.API_HOST,
        port: process.env.API_PORT
    },
    database: {
        host: process.env.DATABASE_HOST,
        port: process.env.DATABASE_PORT,
        name: process.env.DATABASE_NAME,
        user: process.env.DATABASE_USER,
        pass: process.env.DATABASE_PASS
    },
    authServer: {
        url: process.env.AUTH_SERVER_URL
    },
    JWT_SECRET_KEY: process.env.JWT_SECRET_KEY,
    LOGIN_EXPIRES_IN_SECONDS: process.env.LOGIN_EXPIRES_IN_SECONDS,
    SMS_PROVIDER_API_KEY: process.env.SMS_PROVIDER_API_KEY
};

const configSchema = Joi.object({
    environment: Joi.string()
        .valid('development', 'test', 'production')
        .default('production'),
    api: {
        host: Joi.string().default("0.0.0.0"),
        port: Joi.number().default(3000)
    },
    database: {
        host: Joi.string().default("localhost"),
        port: Joi.number().default(5432),
        name: Joi.string().default("team_coaching"),
        user: Joi.string().required(),
        pass: Joi.string().required()
    },
    authServer: {
        url: Joi.string()
    },
    JWT_SECRET_KEY: Joi.string().default(`super_secret`),
    LOGIN_EXPIRES_IN_SECONDS: Joi.number().default(15552000000), // a half year,
    SMS_PROVIDER_API_KEY: Joi.string()
});

const { error: validationError, value: validatedConfig } = configSchema.validate(config);
if (validationError) {
    throw new Error(`Configuration validation error: ${validationError.message}`);
}

interface AppConfig {
    readonly environment: 'development' | 'test' | 'production';
    readonly api: AppConfigApi;
    readonly database: AppConfigDatabase;
    readonly authServer: AppConfigAuthServer;
    readonly JWT_SECRET_KEY: string;
    readonly LOGIN_EXPIRES_IN_SECONDS: number;
    readonly SMS_PROVIDER_API_KEY: string;
}

interface AppConfigApi {
    readonly host: string;
    readonly port: number;
}

interface AppConfigDatabase {
    readonly host: string;
    readonly port: number;
    readonly name: string;
    readonly user: string;
    readonly pass: string;
}

interface AppConfigAuthServer {
    readonly url: string;
}

export default validatedConfig as AppConfig;
