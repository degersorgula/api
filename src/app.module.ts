import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServeStaticModule } from '@nestjs/serve-static';
import config from './app.config';
import { User } from './entities';
import { AuthModule } from './modules/auth.module';
import { join } from 'path';
import { File } from './entities/file.entity';
import { Image } from './entities/image.entity';
import { Entry } from './entities/entry.entity';
import { EntryModule } from './modules/entry.module';
import { FileModule } from './modules/file.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: config.database.host,
      port: config.database.port,
      username: config.database.user,
      password: config.database.pass,
      database: config.database.name,
      // keepConnectionAlive: true,
      // timezone: 'Z', // Use UTC
      // debug: config.environment == 'development',
      // trace: true,
      logging: config.environment === 'development' ? 'all' : ['warn', 'error'],
      logger: config.environment === 'development' ? 'advanced-console' : 'debug',
      entities: [
        User,
        File,
        Image,
        Entry
      ],
      synchronize: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'static'),
      serveRoot: '/static'
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', '.well-known'),
      serveRoot: '/.well-known'
    }),
    AuthModule,
    EntryModule,
    FileModule
  ],
  providers: [],
})
export class AppModule { }
