import { SMSService } from './../services/sms.service';
import { AuthService } from './../services/auth.service';
import { User } from './../entities/user.entity';
import { Controller, Post, Body, Get, UnauthorizedException, ForbiddenException } from '@nestjs/common';
import { ApiConsumes, ApiProduces, ApiBody} from '@nestjs/swagger';
import { memoryCache } from '../tools';

@Controller('auth')
@ApiConsumes('application/json')
@ApiProduces('application/json')
export class AuthController {
  constructor(
    private authService: AuthService,
    private smsService: SMSService
  ) {}

  @Get()
  async auth(){
    return `Welcome auth endpoint`;
  }
  
  @Post('signIn')
  @ApiBody({ type: User })
  async signIn(@Body() user: User): Promise<{token: string}> {
    const token = await this.authService.sign(user);
    return {token};
  }

  @Post('register')
  @ApiBody({ type: User })
  async register(@Body() user: User): Promise<string> {
    return await this.authService.register(user);
  }

  @Post('signOut')
  signOut(): string {
    return `Sign out!`;
  }

  @Post('singleToken')
  async singleToken(@Body() payload: {
    phone: string
  }) {
    this.smsService.validatePhone(payload.phone);
    const key = `SSO:${payload.phone}`;
    const store: {
      token: string,
      count: number
    } = memoryCache.get(key);
    let token = '';
    let count;

    if (!store) {
      const digits = '0123456789'; 
      for (let i = 0; i < 6; i++ ) { 
          token += digits[Math.floor(Math.random() * 10)]; 
      } 
      count = 0;
    } else {
      token = store.token;
      count = store.count + 1;
    }

    if ( count >= 10 ) {
      throw new ForbiddenException(`Exceed the sms limit`);
    }

    memoryCache.set(key, {
      token,
      count,
    }, 240);
    
    await this.smsService.sendSMS({
      message: encodeURI(`Değer Sorgula 6 haneli giriş kodunuz: ${token} \n\nwww.degersorgula.com`),
      phone: payload.phone
    });

    return { key };
  }

  @Post('singleSign')
  async singleSign(@Body() payload) {
    const key = `SSO:${payload.phone}`;
    const store: {
      token: string,
      count: number
    } = memoryCache.get(key);

    if (!store || !store.token || store.token  !== payload.token) {
      throw new UnauthorizedException();
    }

    const authToken = await this.authService.signOrRegister({
      phone: payload.phone,
    } as User);

    return { token: authToken };
  }
}
