import { EntryService } from './../services/entry.service';
import { FileService } from './../services/file.service';
import { User } from './../decorators/user.decorator';
import { Entry } from './../entities/entry.entity';
import { Controller, Post, Body, UseGuards } from '@nestjs/common';
import { ApiConsumes, ApiProduces } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { ImageService } from 'src/services/image.service';

@Controller('entry')
@ApiConsumes('application/json')
@ApiProduces('application/json')
@UseGuards(AuthGuard('bearer'))
export class EntryController {
  constructor(
    private imageService: ImageService,
    private fileService: FileService,
    private entryService: EntryService
  ) { }

  @Post('new')
  async singleSign(@Body() payload: Entry, @User() user) {

    const images = [];
    if (payload.images) {
      for (const image of payload.images) {
        const imageInstance = await this.imageService.create(image);
        images.push(imageInstance);
      }
    }
    
    const files = [];
    if (payload.files) {
      for (const file of payload.files) {
        const fileInstance = await this.fileService.create(file);
        files.push(fileInstance);
      }
    }

    const entry = new Entry({
      fullname: payload.fullname,
      tckno: payload.tckno,
      brand: payload.brand,
      year: payload.year,
      model: payload.model,
      crashDate: payload.crashDate,
      plaque: payload.plaque,
      user,
      images,
      files
    });

    return await this.entryService.create(entry);
  }

}
