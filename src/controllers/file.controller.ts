import { Controller, Post, UseGuards, UseInterceptors, UploadedFiles, NotAcceptableException } from '@nestjs/common';
import { ApiConsumes, ApiProduces, ApiBody } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { FilesInterceptor } from '@nestjs/platform-express';
import { outputFileSync } from 'fs-extra';
import { join } from 'path';
import { v4 as uuidv4 } from 'uuid';
import * as FileType from 'file-type';

@Controller('file')
@ApiConsumes('application/json')
@ApiProduces('application/json')
@UseGuards(AuthGuard('bearer'))
export class FileController {

  @Post('image')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: 'multipart/form-data',
    required: true,
    schema: {
      type: 'object',
      properties: {
        ['files']: {
          type: 'array',
          items: {
            type: 'string',
            format: 'binary',
          },
        },
      },
    },
  })
  @UseInterceptors(FilesInterceptor('images'))
  async uploadImage(@UploadedFiles() files) {

    const supportedTypes = ['pdf', 'docx', 'jpg', 'png', 'jp2', 'jpm', 'jpx']; 
    const isSupported = (type) => {
      return !!supportedTypes.find(_type => _type === type)
    }

    const successFullFiles = [];

    for (const file of files) {
      const type = await FileType.fromBuffer(file.buffer);
      if (isSupported(type.ext)) {
        const uui = uuidv4();
        const fileName = `${uui}.${type.ext}`;
        const staticFileDirectory = join(__dirname, '..', '..', 'static', 'images', fileName );
        await outputFileSync(staticFileDirectory, file.buffer);
        successFullFiles.push(fileName);
      } else {
        throw new NotAcceptableException(`File format is not supported ${type.ext}`)
      }
    }

    return successFullFiles.map((file) => `/static/images/${file}`);
  }


  @Post('document')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: 'multipart/form-data',
    required: true,
    schema: {
      type: 'object',
      properties: {
        ['files']: {
          type: 'array',
          items: {
            type: 'string',
            format: 'binary',
          },
        },
      },
    },
  })
  @UseInterceptors(FilesInterceptor('files'))
  async uploadFile(@UploadedFiles() files) {

    const supportedTypes = ['pdf', 'docx', 'jpg', 'png', 'jp2', 'jpm', 'jpx']; 
    const isSupported = (type) => {
      return !!supportedTypes.find(_type => _type === type)
    }

    const successFullFiles = [];

    for (const file of files) {
      const type = await FileType.fromBuffer(file.buffer);
      if (isSupported(type.ext)) {
        const uui = uuidv4();
        const fileName = `${uui}.${type.ext}`;
        const staticFileDirectory = join(__dirname, '..', '..', 'static', 'files', fileName );
        await outputFileSync(staticFileDirectory, file.buffer);
        successFullFiles.push(fileName);
      } else {
        throw new NotAcceptableException(`File format is not supported ${type.ext}`)
      }
    }

    return successFullFiles.map((file) => `/static/files/${file}`);
  }

}
