import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';

@Controller('index')
@UseGuards(AuthGuard('bearer'))
@ApiBearerAuth()
export class HelloController {
  
  @Get()
  getHello(): string {
    return `Running`;
  }
}
