import { DeepPartial, CreateDateColumn, UpdateDateColumn, PrimaryGeneratedColumn } from "typeorm";
import { DbDataType } from "./db-data-type";

export class BaseEntity {
  protected constructor(input?: DeepPartial<BaseEntity>) {
    if (input) {
      for (const [key, value] of Object.entries(input)) {
        (this as any)[key] = value;
      }
    }
  }

  @PrimaryGeneratedColumn('increment', { type: DbDataType.Int })
  id: number;

  @CreateDateColumn() createdAt: Date;

  @UpdateDateColumn() updatedAt: Date;
}
