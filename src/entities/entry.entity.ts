import { User } from './user.entity';
import { DeepPartial, ManyToOne, OneToMany } from 'typeorm';
import { Entity, Column } from 'typeorm';
import { BaseEntity } from './base.enitity';
import { DbDataType } from './db-data-type';
import { Image } from './image.entity';
import { File } from './file.entity';

/**
 * Contains information about a user.
 */
@Entity('entry')
export class Entry extends BaseEntity {
    constructor(input?: DeepPartial<Entry>) {
        super(input);
    }

    @Column(DbDataType.String)
    fullname: string;

    @Column(DbDataType.String)
    tckno: string;

    @Column(DbDataType.String)
    plaque: string

    @Column(DbDataType.String)
    year: string;

    @Column(DbDataType.String)
    brand: string;

    @Column(DbDataType.String)
    model: string;

    @Column(DbDataType.String)
    crashDate: string;
    
    @ManyToOne(type => User)
    user: User;

    @OneToMany(type => Image, image => image.entry, {
        eager: true
    })
    images: Image[];

    @OneToMany(type => File, file => file.entry, {
        eager: true
    })
    files: File[];
}
