import { DeepPartial, ManyToOne } from 'typeorm';
import { Entity, Column } from 'typeorm';
import { BaseEntity } from './base.enitity';
import { DbDataType } from './db-data-type';
import { Entry } from './entry.entity';

/**
 * Contains information about a user.
 */
@Entity('file')
export class File extends BaseEntity {
    constructor(input?: DeepPartial<File>) {
        super(input);
    }

    @Column(DbDataType.String)
    path: string;
    
    @ManyToOne(type => Entry)
    entry: Entry;
}
