import { DeepPartial, OneToMany } from 'typeorm';
import { Entity, Column } from 'typeorm';
import { BaseEntity } from './base.enitity';
import { DbDataType } from './db-data-type';
import { Entry } from './entry.entity';

/**
 * Contains information about a user.
 */
@Entity('user')
export class User extends BaseEntity {
    constructor(input?: DeepPartial<User>) {
        super(input);
    }

    @Column(DbDataType.String)
    phone: string;

    @OneToMany(type => Entry, entry => entry.user, {
        eager: true
    })
    entries: Entry[];
}
