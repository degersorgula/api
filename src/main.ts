import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import config from './app.config';
import { AppModule } from './app.module';
import * as typeOrmExt from './tools/typeorm-ext';
import * as fs from 'fs';
import { NestApplicationOptions } from '@nestjs/common';

// Initialize the module
typeOrmExt.init();

async function bootstrap() {
    const appOptions: NestApplicationOptions = {
        cors: true
    };

    // if (config.environment == 'production' || config.environment == 'test') {
    //     const key  = fs.readFileSync(__dirname + `/../certs/${config.environment}/key.pem`, 'utf8');
    //     const cert = fs.readFileSync(__dirname + `/../certs/${config.environment}/cert.pem`, 'utf8');
    //     const ca = fs.readFileSync(__dirname + `/../certs/${config.environment}/chain.pem`, 'utf8');
        
    //     console.log(config.environment)
        
    //     appOptions.httpsOptions = {
    //         key,
    //         cert,
    //         ca
    //     }
    // }
    
    const app = await NestFactory.create(AppModule, appOptions);

    app.setGlobalPrefix('api/v1');

    if (config.environment == 'development' || config.environment == 'test') {
        // Swagger
        const options = new DocumentBuilder()
            .setTitle('Degersorgula API')
            .setVersion('1.0')
            .setBasePath('api/v1')
            .addBearerAuth()
            .build();
    
        const document = SwaggerModule.createDocument(app, options);
        SwaggerModule.setup('swagger', app, document);
    }

    await app.listen(config.api.port, config.api.host)
    
}
bootstrap();
