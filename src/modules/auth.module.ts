import { AuthController } from './../controllers/auth.controller';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { HttpBearerAuthStrategy } from '../security';
import { HubModule } from './hub.module';

@Module({
    imports: [PassportModule, HubModule],
    providers: [HttpBearerAuthStrategy],
    exports: [HttpBearerAuthStrategy],
    controllers: [
        AuthController
    ]
})
export class AuthModule { }
