import { HubModule } from './hub.module';
import { EntryController } from './../controllers/entry.controller';
import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';

@Module({
    imports: [
        PassportModule,
        HubModule
    ],
    providers: [],
    exports: [],
    controllers: [
        EntryController
    ]
})
export class EntryModule { }
