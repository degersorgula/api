import { FileController } from '../controllers/file.controller';
import { HubModule } from './hub.module';
import { PassportModule } from '@nestjs/passport';
import { Module } from '@nestjs/common';

@Module({
    imports: [
        PassportModule,
        HubModule
    ],
    providers: [],
    exports: [],
    controllers: [
        FileController
    ]
})
export class FileModule { }
