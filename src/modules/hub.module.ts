import { Entry } from './../entities/entry.entity';
import { EntryService } from './../services/entry.service';
import { Image } from './../entities/image.entity';
import { File } from './../entities/file.entity';
import { ImageService } from './../services/image.service';
import { FileService } from './../services/file.service';
import { SMSService } from './../services/sms.service';
import { HelloController } from './../controllers/hello.controller';
import { AuthService } from 'src/services/auth.service';
import { User } from './../entities/user.entity';
import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserService } from 'src/services';
import { JwtModule } from '@nestjs/jwt';
import appConfig from 'src/app.config';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      File,
      Image,
      Entry
    ]),
    JwtModule.register({
      secretOrPrivateKey: appConfig.JWT_SECRET_KEY,
      signOptions: {
        expiresIn: appConfig.LOGIN_EXPIRES_IN_SECONDS,
      },
    }),
  ],
  providers: [
    UserService,
    AuthService,
    SMSService,
    FileService,
    ImageService,
    EntryService
  ],
  exports: [
    UserService,
    AuthService,
    SMSService,
    FileService,
    ImageService,
    EntryService
  ],
  controllers: [
    HelloController
  ]
})
export class HubModule {

}
