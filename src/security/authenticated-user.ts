/**
 * Contains information about the authenticated user.
 */
export interface AuthenticatedUser {
    /**
     * The ID of the user.
     */
    readonly id: number;
}
