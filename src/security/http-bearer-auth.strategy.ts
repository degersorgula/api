import { AuthService } from 'src/services/auth.service';
import { User } from './../entities/user.entity';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-http-bearer';
import { memoryCache } from '../tools';
import { AuthenticatedUser } from './authenticated-user';
import { UserService } from '../services';

@Injectable()
export class HttpBearerAuthStrategy extends PassportStrategy(Strategy, 'bearer') {
    constructor(
        private readonly userService: UserService,
        private authService: AuthService,
    ) { super(); }

    async validate(token: string): Promise<AuthenticatedUser> {
        const cacheKey = 'Auth:Token:' + token;
        let result: AuthenticatedUser | undefined = memoryCache.get(cacheKey);

        if (result) {
            return result;
        }

        try {
            const userData: User = await this.authService.verifyToken(token);
            const user = await this.userService.findOne(userData);
            
            if (user.id === userData.id) {
                result = {
                    id: user.id
                }
            } else {
                throw new UnauthorizedException();
            }
        } catch {
            throw new UnauthorizedException();
        }

        // Cache the user for 30m
        memoryCache.set(cacheKey, result, 60 * 60);

        return result;
    }
}
