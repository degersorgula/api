import { User } from './../entities/user.entity';
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { UserService } from './user.service';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private userService: UserService
  ) {}

  async register(payload: User): Promise<string> {
    const user = await this.userService.createUser(payload);

    return await this.signToken(user);
  }

  async sign(payload: User): Promise<string> {
    const user: User = await this.userService.findOne(payload);
    
    if (!user) {
      throw new UnauthorizedException(`User not found`);
    }
    
    return await this.signToken({...user});
  }

  async signToken(payload: User): Promise<string> {
    return await this.jwtService.sign(payload);
  }

  async verifyToken(token: string): Promise<User> {
    const user = await this.jwtService.verify(token);

    return user;
  }

  async signOrRegister(payload: User): Promise<string> {
    const user = await this.userService.findOne(payload);
    if (!user) {
      return await this.register(payload);
    }
    
    return this.sign(user);
  }
}
