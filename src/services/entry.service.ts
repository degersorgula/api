import { Entry } from './../entities/entry.entity';
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

@Injectable()
export class EntryService {
  constructor(
    @InjectRepository(Entry)
    private readonly entryRepository: Repository<Entry>
  ) {}

  async create(entry: Entry) {
    const entryInstance = await this.entryRepository.create(entry);
    return await this.entryRepository.save(entryInstance);
  }
}
