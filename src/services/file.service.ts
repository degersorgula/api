import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { File } from '../entities';

/**
 * Provides access to user data.
 */
@Injectable()
export class FileService {
    constructor(
        @InjectRepository(File)
        private readonly fileRepository: Repository<File>
    ) { }

    async findOne(file: File) {
        return await this.fileRepository.findOne({ where: file });
    }

    async create(file: File) {
      const fileInstance = await this.fileRepository.create(file); 
      return await this.fileRepository.save(fileInstance);
    }
}
