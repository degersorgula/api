import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Image } from '../entities';

/**
 * Provides access to user data.
 */
@Injectable()
export class ImageService {
    constructor(
        @InjectRepository(Image)
        private readonly imageRepository: Repository<Image>
    ) { }

    async findOne(image: Image) {
        return await this.imageRepository.findOne({ where: image });
    }

    async create(image: Image) {
      const imageInstance = await this.imageRepository.create(image); 
      return await this.imageRepository.save(imageInstance);
    }
}
