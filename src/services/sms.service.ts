import { Injectable, NotAcceptableException } from "@nestjs/common";
import fetch from 'node-fetch';
import appConfig from "src/app.config";
import * as JoiDefault from '@hapi/joi';
import * as JOIPhone from 'joi-phone-number';

const Joi = JoiDefault.extend(JOIPhone)

type SendSMS = { 
  message: string; 
  phone: string
};

@Injectable()
export class SMSService {

  private host = 'http://api.v2.masgsm.com.tr';

  async post(endpoint, body) {
    const url = new URL(`${this.host}${endpoint}`);

    const response = await fetch(url, {
      method: 'post',
      headers: {
        'Authorization': `Key ${appConfig.SMS_PROVIDER_API_KEY}`,
      },
      body
    });
    return response.json();
  }

  async get(endpoint) {
    const url = new URL(`${this.host}${endpoint}`);

    const response = await fetch(url, {
      method: 'GET',
      headers: {
        'Authorization': `Key ${appConfig.SMS_PROVIDER_API_KEY}`,
      }
    });

    return response.json();
  }

  async getBalance() {
    return await this.get(`/v2/get/balance`)
  }

  async originators() {
    return await this.get(`/v2/get/originators/detailed`);
  }

  async sendSMS(payload: SendSMS) {
    const { phone, message } = payload;
    const requestOptions = {
      method: 'GET',
    };
    
    return fetch(`http://panel.vatansms.com/panel/smsgonder1N.php?kno=38465&kul_ad=905434377378&sifre=78WCTQ73&gonderen=8505908342&mesaj=${message}&numaralar=${phone}&tur=Turkce`, requestOptions);
  }

  validatePhone (phone: string) {
    if (phone[0] !== '5') {
      throw new NotAcceptableException(`Phone is not valid`);
    }

    const result = Joi.string().phoneNumber({ defaultCountry: 'TR', strict: true}).validate(phone);
    if (result.error) {
      throw new NotAcceptableException(result.error);
    }

    return true;
  }
} 
