import { Injectable, ConflictException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Not } from 'typeorm';
import { User } from '../entities';

/**
 * Provides access to user data.
 */
@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>
    ) { }

    async createUser(userData: User): Promise<User> {
        const userId = await this.userRepository.findOne({ where: userData, select: ['id'] });

        if (userId) {
            throw new ConflictException(`Already exist`);
        }

        const userInstance = await this.userRepository.create(userData);
        const user = await this.userRepository.save(userInstance);

        return user;
    }

    async findOne(user: User) {
        return await this.userRepository.findOne({ where: user, select: ['phone', 'id']});
    }
}
