import { UpdateResult } from "typeorm";

/**
 * Helper methods and bug fixes for `typeorm`.
 * 
 * Extension methods guide: https://kimsereyblog.blogspot.com/2017/09/create-type-extensions-in-typescript.html
 */

declare module 'typeorm' {
    export interface UpdateResult {
        getAffectedRows(): number;
    }
}

/**
 * Contains the fields of a raw result.
 */
interface RawResult {
    affectedRows: number;
    changedRows: number;
    fieldCount: number;
    insertId: number;
}

/**
 * Initializes the module.
 */
export function init(): void {
    /**
     * Addresses an issue with `typeorm`: `UpdateResult.AffectedRows` is `undefined`.
     * https://github.com/typeorm/typeorm/issues/2415
     */
    UpdateResult.prototype.getAffectedRows = function (this: UpdateResult) {
        if (this.affected != undefined) {
            // Does not work with MySQL
            return this.affected;
        } else {
            // MySQL
            return (this.raw as RawResult).affectedRows;
        }
    }
}
